#include <cstdlib>
#include <cerrno>
#include <iostream>
#include <sys/dispatch.h>
#include "Message.hpp"
#include "Receiver.hpp"

Receiver::Receiver(std::string const& name) :
		attach_point(name_attach(NULL, name.c_str(), NAME_FLAG_ATTACH_GLOBAL)) {
	if (this->attach_point) {
		std::cerr << "File receiver attached at /dev/name/global/" << name
				<< std::endl;
	}
}

Receiver::~Receiver() {
	name_detach(attach_point, 0);
}

void Receiver::assert(bool value) {
	if (!value) {
		std::cerr << strerror(errno) << std::endl;
		exit (errno);
	}
}

int Receiver::run() {
	this->assert(this->attach_point);
	char io_header[4000];
	_msg_info info;
	int status = MsgReceive(attach_point->chid, io_header, sizeof(io_header), &info);
	MsgReply(status, EOK, NULL, 0);

	Message *msg = new Message { 4 };
	status = MsgReceive(attach_point->chid, msg->data.data(), msg->data.size(), NULL);
	this->assert(status > 0);
	int lenn = msg->length;
	std::cerr << "Data packages size (bytes): " << lenn << std::endl;
	status = MsgReply(status, EOK, nullptr, 0);
	this->assert(status >= 0);
	delete msg;

	do {
		msg = new Message { lenn };
		_msg_info msginf;
		status = MsgReceive(attach_point->chid, msg->data.data(), msg->data.size(), &msginf);
		if (msginf.msglen != msg->data.size()) {
			status = MsgReply(status, RECEIVE_FAILED, nullptr, 0);
			std::cerr << "Message is too large to be received." << std::endl;
			exit(0);
		}
		this->assert(status > 0);

		if (msg->length > 0) {
			std::cerr << "Received #" << msg->offset << "+" << msg->length << " bytes" << std::endl;
			writer.write(msg);
		}

		status = MsgReply(status, EOK, nullptr, 0);
		this->assert(status >= 0);
	} while (msg->length > 0);

	writer.finish();
	return EXIT_SUCCESS;
}
