#pragma once
#include <fstream>
#include <vector>
#define MESSAGE_SIZE 1024

/**
 * Message class.
 */
class Message {

public:

	/**
	 * Construct empty message.
	 * @param size of payload
	 */
	Message(int size);

	/**
	 * Destruct message.
	 */
	virtual ~Message();

	/**
	 * Raw message data.
	 */
	std::vector<char> data;

	/**
	 * File size in bytes.
	 */
	int &length;

	/**
	 * Offset size in bytes.
	 */
	int &offset;

	class GreaterOffsetComparator {

	public:

		/**
		 * Check is first message come after second message.
		 * @param a First message.
		 * @param b Second message.
		 * @return a.offset > b.offset
		 */
		bool operator()(Message *const& a, Message *const& b);

	};
};
