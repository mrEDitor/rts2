#include "Writer.hpp"
#include <algorithm>
#include <iostream>

Writer::Writer()
		: runner(std::bind(&Writer::run, this)), eof(false) {
}

Writer::~Writer() {
	runner.join();
}

std::string Writer::format_size(int size) {
	std::string prefix[] = { "B", "KiB", "MiB", "GiB" };
	int prefix_number = 0;
	while (size > 4096) {
		size /= 1024;
		++prefix_number;
	}
	return std::to_string(size) + ' ' + std::string(prefix[prefix_number]);
}

void Writer::write(Message *chunk) {
	std::unique_lock<std::mutex> lock_it(lock);
	messages.push(chunk);
	cv.notify_all();
}

void Writer::finish() {
	std::unique_lock<std::mutex> lock_it(lock);
	eof = true;
	cv.notify_all();
}

void Writer::run() {
	for (int i = 0; !eof || messages.size();) {
		std::unique_lock<std::mutex> lock_it(lock);
		while ((messages.empty() && !eof) || (messages.size() && messages.top()->offset != i)) {
			cv.wait(lock_it);
		}
		if (messages.size())
		{
			Message *chunk = messages.top();
			messages.pop();
			std::cout.write(chunk->data.data(), chunk->length);
			i += chunk->length;
		}
	}
	std::cerr << "File received." << std::endl;
}
