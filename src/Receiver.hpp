#pragma once
#include <string>
#include <sys/dispatch.h>
#include "Writer.hpp"

/**
 * Receiver application class.
 * Listens for incoming files.
 */
class Receiver {

public:
	/**
	 * Error reply code.
	 */
	static const int RECEIVE_FAILED = 12;

	/**
	 * Construct receiver.
	 * @param name /dev/name/local/NAME.
	 */
	Receiver(std::string const& name);

	/**
	 * Destruct receiver.
	 */
	virtual ~Receiver();

	/**
	 * Run receiver, listen for messages.
	 * @return Exit code.
	 */
	int run();

private:

	/**
	 * Assert argument to true, crash otherwise.
	 * @param value Value to assert.
	 * @return never
	 */
	void assert(bool value);

	/** Attachment point. */
	name_attach_t *attach_point;

	/** Async writer object. */
	Writer writer;

};
