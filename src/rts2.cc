#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include "Receiver.hpp"
#include "Sender.hpp"

#define TAB "\t"
#define STR_EQ 0
#define MESSAGE_SIZE 1024

#define NAME_LENGTH 8

/**
 * Generate random alpha-numeric name.
 * @return std::string
 */
std::string generate_name();

/**
 * Validate /dev/name/local/NAME.
 * @param name NAME string.
 * @return Whether name valid.
 */
bool is_name_valid(std::string const& name);

/**
 * Entrance point.
 * @param argc Arguments count.
 * @param argv Arguments list.
 * @return Error code.
 */
int main(int argc, char *argv[]) {
	srand(time(nullptr));

	/** Package length */
	int bytes_per_package = MESSAGE_SIZE;

	/** Define name under /dev/name/local/... */
	std::string name_to_attach = generate_name();

	switch (argc) {

	case 3: /* rts2 receive name */
		name_to_attach = argv[2];
		if (!is_name_valid(name_to_attach)) {
			break;
		}
		/* no break */

	case 2: /* rts2 receive */
		if (strcmp(argv[1], "receive") == STR_EQ) {
			Receiver receiver(name_to_attach);
			return receiver.run();
		}
		break;

	case 4: /* rts2 send host bytes_per_package */
		bytes_per_package = strtol(argv[3], nullptr, 10);
		if (strcmp(argv[1], "send") == STR_EQ && bytes_per_package > 0) {
			Sender sender(argv[2], bytes_per_package);
			return sender.run();
		}
		break;

	}

	std::cout << "rts2 qnet file share utility" << std::endl;
	std::cout << "Usage:" << std::endl;
	std::cout << TAB << argv[0] << " receive [<name>]" << std::endl;
	std::cout << TAB << argv[0]
			<< " send <receiver-name> <bytes-per-package>"
			<< std::endl;
	return EXIT_SUCCESS;
}

std::string generate_name() {
	std::string name;
	for (int i = 0; i < NAME_LENGTH; ++i) {
		char c = (char) rand() % 36;
		c += (c < 10) ? '0' : 'a' - 10;
		name.push_back(c);
	}
	return name;
}

bool is_name_valid(std::string const& name) {
	for (char c : name) {
		if (!std::isalnum(c)) {
			return false;
		}
	}
	return true;
}
