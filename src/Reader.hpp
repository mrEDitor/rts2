#pragma once
#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>
#include "Message.hpp"
#define BUFFER_SIZE 60

/**
 * Reader thread class.
 */
class Reader {

public:

	/**
	 * Constuct reader.
	 */
	Reader(int bytes_per_package);

	/**
	 * Destruct reader.
	 */
	virtual ~Reader();

	/**
	 * Announce file.
	 * @return File announcement as Message.
	 */
	Message announce();

	/**
	 * Read next part of file.
	 * @return Part of file as Message or null if end of file was reached.
	 */
	Message* read();

private:

	/**
	 * Run async reading.
	 */
	void run();

	/** Bytes count per package. */
	int bytes_per_package;

	/** Messages to send. */
	std::vector<Message*> messages;

	/** Was file announced? */
	bool announced;

	/** Sync variable. */
	std::condition_variable cv;

	/** Mutex. */
	std::mutex lock;

	/** Thread object. */
	std::thread runner;

};
