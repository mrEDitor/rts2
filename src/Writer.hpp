#pragma once
#include <condition_variable>
#include <fstream>
#include <queue>
#include <string>
#include <thread>
#include "Message.hpp"

/**
 * Writer thread class.
 */
class Writer {

public:

	/**
	 * Construct writer.
	 */
	Writer();

	/**
	 * Destruct writer.
	 */
	virtual ~Writer();

	/**
	 * Write chunk of file.
	 * @param chunk Chunk to write.
	 * @return void
	 */
	void write(Message *chunk);

	/**
	 * Finish flag setter.
	 * @return void
	 */
	void finish();

private:

	/**
	 * Format file size to human-readable form.
	 * @param size Size in bytes.
	 * @return Tagged file size.
	 */
	std::string format_size(int size);

	/**
	 * Writer working thread body.
	 * @return void
	 */
	void run();

	/** Message queue. */
	std::priority_queue<Message*, std::vector<Message*>,
			Message::GreaterOffsetComparator> messages;

	/** Sync variable. */
	std::condition_variable cv;

	/** Mutex. */
	std::mutex lock;

	/** End of file. */
	bool eof;

	/** Thread object. */
	std::thread runner;

};
