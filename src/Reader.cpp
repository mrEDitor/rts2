#include "Reader.hpp"
#include <algorithm>
#include <iostream>

Reader::Reader(int bytes_per_package)
	: bytes_per_package(bytes_per_package),
	  announced(false),
	  runner(std::bind(&Reader::run, this)) {
}

Reader::~Reader() {
	runner.join();
}

Message Reader::announce() {
	Message msg(4);
	msg.length = bytes_per_package;
	strcpy(msg.data.data(), "");
	announced = true;
	cv.notify_all();
	return msg;
}

Message* Reader::read() {
	std::unique_lock < std::mutex > lock_it(lock);
	while (messages.size() < BUFFER_SIZE) {
		if (std::cin.eof() && messages.empty()) {
			return nullptr;
		} else if (std::cin.eof() && !messages.empty()) {
			break;
		} else {
			cv.wait(lock_it);
		}
	}
	std::random_shuffle(messages.begin(), messages.end());
	// std::swap(messages[rand() % messages.size()], messages.back()); // for wat
	Message *msg = messages.back();
	messages.pop_back();
	return msg;
}

void Reader::run() {
	while (!announced) {
		std::unique_lock < std::mutex > lock_it(lock);
		cv.wait(lock_it);
	}
	for (int i = 0; !std::cin.eof(); ) {
		std::unique_lock < std::mutex > lock_it(lock);
		Message *msg = new Message(bytes_per_package);
		msg->offset = i;
		std::cin.read(msg->data.data(), bytes_per_package);
		i += (msg->length = std::cin.gcount());
		messages.push_back(msg);
		cv.notify_all();
	}
}
