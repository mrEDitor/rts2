#include "Message.hpp"
#include <iostream>

Message::Message(int size) :
	data(size + 3 * sizeof(int)),
	length(((int*)(data.data() + size))[1]),
	offset(((int*)(data.data() + size))[2]) {
}

Message::~Message() {
}

bool Message::GreaterOffsetComparator::operator() (Message *const& a, Message *const& b) {
	return a->offset > b->offset;
}

