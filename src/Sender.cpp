#include <cstdlib>
#include <iostream>
#include "Message.hpp"
#include "Receiver.hpp"
#include "Sender.hpp"
#include <sys/dispatch.h>

Sender::Sender(std::string const& receiver, int bytes_per_package) :
		reader(bytes_per_package), receiver(
				name_open(receiver.c_str(), NAME_FLAG_ATTACH_GLOBAL)) {
	this->assert(this->receiver >= 0);
}

Sender::~Sender() {
}

void Sender::assert(bool value) {
	if (!value) {
		std::cerr << strerror(errno) << std::endl;
		exit (errno);
	}
}

int Sender::run() {
	Message announcement = reader.announce();
	std::cout << "Sending announcement: package length=" << announcement.length << std::endl;
	int status = MsgSend(this->receiver, announcement.data.data(), announcement.data.size(), nullptr, 0);
	this->assert(status >= 0);

	Message *msg;
	while (msg = reader.read()) {
		std::cout << "Sending #" << msg->offset << "+" << msg->length << " bytes" << std::endl;
		status = MsgSend(this->receiver, msg->data.data(), msg->data.size(), nullptr, 0);
		std::cerr << "sent " << msg->data.size() << std::endl;
		if (status == Receiver::RECEIVE_FAILED) {
			std::cout << "Receiver rejected message. Try to decrease bytes_per_package value." << std::endl;
			return EXIT_FAILURE;
		}
		this->assert(status >= 0);
		delete msg;
	}

	Message ennouncement { announcement.length };
	ennouncement.length = 0;
	std::cout << "Sending EOF signal" << std::endl;
	status = MsgSend(this->receiver, ennouncement.data.data(), ennouncement.data.size(), nullptr, 0);
	this->assert(status >= 0);

	return EXIT_SUCCESS;
}
