#pragma once
#include <string>
#include "Reader.hpp"

/**
 * Sender application class.
 * Sends given file to given machine over qnet network.
 */
class Sender {

public:

	/**
	 * Construct sender.
	 * @param receiver			Receiver hostname.
	 * @param bytes_per_package Bytes per package in file.
	 */
	Sender(
			std::string const& receiver,
			int bytes_per_package
	);

	/**
	 * Destruct sender.
	 */
	virtual ~Sender();

	/**
	 * Run sender, send file.
	 * @return Exit code.
	 */
	int run();

private:

	/** Assert value to true. */
	void assert(bool value);

	/** Receiver resource ID. */
	int receiver;

	/** File reader. */
	Reader reader;

};
